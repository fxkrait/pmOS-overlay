#Power button

#If you want to make use of the power button, bind XF86PowerOff to this script:

FILE=~/.screenoff
if [ -f $FILE ]; then
    echo 0 > /sys/devices/platform/omapdrm.0/graphics/fb0/blank
    rm ~/.screenoff
else
    echo 1 > /sys/devices/platform/omapdrm.0/graphics/fb0/blank
    touch ~/.screenoff
fi

#You cannot use this to turn the screen back on if i3lock or similar is running, as they would grab the keys

